Changelog
=========

All notable changes to the Commerce Viva Wallet module.

## 1.0.1 (2022-09-27):

### Fixed

* Access check for callback routes


## 1.0.0 (2022-09-26):

### Fixed

* API credentials section in README


## 1.0.0-alpha2 (2022-08-04):

### Fixed

* $middleware variable type in ClientFactory constructor


## 1.0.0-alpha1 (2022-08-04):

First alpha release.
