Commerce Viva Wallet
====================

Provides a Viva Wallet payment gateway for Drupal Commerce.


## Requirements

* [Drupal commerce](https://www.drupal.org/project/commerce)


## Installation

First you have to download the module and its dependencies. The easiest way to do this is using Composer:

```
composer require drupal/commerce_vivawallet
```

Now enable the module as usual and the Viva Wallet payment gateway should become available.


## Configuration

### Payment source

Create a payment source as described in the [Viva Wallet documentation](https://developer.vivawallet.com/getting-started/create-a-payment-source/payment-source-for-online-payments/).

Set the success URL to  `/commerce-vivawallet/[payment gateway id]/success` and the failure URL to
`/commerce-vivawallet/[payment gateway id]/cancel`. Please note that `[payment gateway id]` should
be replaced with the ID of the payment gateway to be created.

Leave the integration method and advanced configuration as is and note down the source code.


### API credentials

Get your merchant ID, API key and smart checkout credentials (client ID and secret)
as described on following pages:

* [Find your merchant ID and API key](https://developer.vivawallet.com/getting-started/find-your-merchant-id-and-api-key/)
* [Find your client ID and secret](https://developer.vivawallet.com/getting-started/find-your-client-credentials/)


### Payment gateway

Create a new `Viva Wallet` payment gateway via `/admin/commerce/config/payment-gateways`.

**Important**: Make sure you use the previously specified payment gateway ID!

Optionally you can also enable request logging. If enabled all API requests and responses
will be logged. If disabled only failed calls will be logged.

The desired color should be specified in hexadecimal format (`ff0000` for example).

Please note that your credentials will be saved as configuration and thus be exported by default.
You might want to exclude them via [Config Ignore](https://www.drupal.org/project/config_ignore)
or a similar module.


### Transaction hook

Create a Transaction payment created hook as described in the [Viva Wallet documentation](https://developer.vivawallet.com/webhooks-for-payments/transaction-payment-created/).

Use `https://[your domain]/commerce-vivawallet/[payment gateway id]/hook` as URL, but don't forget to
replace `[your domain]` with the proper domain name and `[payment gateway id]` with the ID of the
payment gateway.
