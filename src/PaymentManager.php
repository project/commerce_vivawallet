<?php

namespace Drupal\commerce_vivawallet;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The payment manager.
 */
class PaymentManager implements PaymentManagerInterface {

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected PaymentStorageInterface $paymentStorage;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $storage */
    $storage = $entity_type_manager->getStorage('commerce_payment');
    $this->paymentStorage = $storage;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByOrderCode(PaymentGatewayInterface $payment_gateway, int $order_code): ?PaymentInterface {
    /** @var int[] $ids */
    $ids = $this->paymentStorage->getQuery()
      ->condition('payment_gateway', $payment_gateway->id())
      ->condition('remote_id', $order_code)
      ->condition('state', 'new')
      ->range(0, 1)
      ->execute();

    if ($ids) {
      return $this->loadPayment(reset($ids));
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByTransactionId(PaymentGatewayInterface $payment_gateway, string $transaction_id): ?PaymentInterface {
    /** @var int[] $ids */
    $ids = $this->paymentStorage->getQuery()
      ->condition('payment_gateway', $payment_gateway->id())
      ->condition('remote_id', $transaction_id)
      ->condition('state', 'new', '<>')
      ->range(0, 1)
      ->execute();

    if ($ids) {
      return $this->loadPayment(reset($ids));
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByOrderCodeOrTransactionId(PaymentGatewayInterface $payment_gateway, int $order_code, string $transaction_id): ?PaymentInterface {
    $payment = $this->loadByOrderCode($payment_gateway, $order_code);

    if (!$payment) {
      $payment = $this->loadByTransactionId($payment_gateway, $transaction_id);
    }

    return $payment;
  }

  /**
   * Load a payment.
   *
   * @param int $id
   *   The payment entity ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity or NULL if not found.
   */
  protected function loadPayment(int $id): ?PaymentInterface {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->paymentStorage->load($id);

    return $payment;
  }

}
