<?php

namespace Drupal\commerce_vivawallet;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides an interface for the payment manager.
 */
interface PaymentManagerInterface {

  /**
   * Load a payment by order code.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param int $order_code
   *   The order code.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity or NULL if not found.
   */
  public function loadByOrderCode(PaymentGatewayInterface $payment_gateway, int $order_code): ?PaymentInterface;

  /**
   * Load a payment by transaction ID.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param string $transaction_id
   *   The transaction ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity or NULL if not found.
   */
  public function loadByTransactionId(PaymentGatewayInterface $payment_gateway, string $transaction_id): ?PaymentInterface;

  /**
   * Load a payment by order code or transaction ID.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param int $order_code
   *   The order code.
   * @param string $transaction_id
   *   The transaction ID.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|null
   *   The payment entity or NULL if not found.
   */
  public function loadByOrderCodeOrTransactionId(PaymentGatewayInterface $payment_gateway, int $order_code, string $transaction_id): ?PaymentInterface;

}
