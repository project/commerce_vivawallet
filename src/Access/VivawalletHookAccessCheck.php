<?php

namespace Drupal\commerce_vivawallet\Access;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_vivawallet\PaymentManagerInterface;
use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access check for the Viva Wallet hook.
 */
class VivawalletHookAccessCheck implements AccessInterface {

  /**
   * The payment manager.
   *
   * @var \Drupal\commerce_vivawallet\PaymentManagerInterface
   */
  protected PaymentManagerInterface $paymentManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\PaymentManagerInterface $payment_manager
   *   The payment manager.
   */
  public function __construct(PaymentManagerInterface $payment_manager) {
    $this->paymentManager = $payment_manager;
  }

  /**
   * Perform the access check.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access check result.
   */
  public function access(PaymentGatewayInterface $payment_gateway, Request $request): AccessResultInterface {
    // Check the payment gateway.
    if (!$payment_gateway->getPlugin() instanceof VivawalletPaymentGatewayInterface) {
      return AccessResult::forbidden('Invalid payment gateway');
    }

    // Decode the content.
    try {
      $content = json_decode($request->getContent(), TRUE, 1024, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $ex) {
      return AccessResult::forbidden('Invalid JSON content');
    }

    // Check the event type ID.
    if (!isset($content['EventTypeId']) || $content['EventTypeId'] !== 1796) {
      return AccessResult::forbidden('Invalid event type ID');
    }

    // Check the order code and transaction ID.
    $event_data = $content['EventData'] ?? [];
    if (!isset($event_data['OrderCode'], $event_data['TransactionId'])) {
      return AccessResult::forbidden('Required event data missing');
    }

    $order_code = $event_data['OrderCode'];
    $transaction_id = $event_data['TransactionId'];

    if (!is_int($order_code) || !Uuid::isValid($transaction_id)) {
      return AccessResult::forbidden('Order code or transaction ID not valid');
    }

    // Check if the payment exists.
    $payment = $this->paymentManager->loadByOrderCodeOrTransactionId(
      $payment_gateway,
      $order_code,
      $transaction_id
    );

    if (!$payment) {
      return AccessResult::forbidden('Nonexistent payment');
    }

    return AccessResult::allowed();
  }

}
