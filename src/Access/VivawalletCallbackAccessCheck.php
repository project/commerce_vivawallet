<?php

namespace Drupal\commerce_vivawallet\Access;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_vivawallet\PaymentManagerInterface;
use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access check for the Viva Wallet callbacks.
 */
class VivawalletCallbackAccessCheck implements AccessInterface {

  /**
   * The payment manager.
   *
   * @var \Drupal\commerce_vivawallet\PaymentManagerInterface
   */
  protected PaymentManagerInterface $paymentManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\PaymentManagerInterface $payment_manager
   *   The payment manager.
   */
  public function __construct(PaymentManagerInterface $payment_manager) {
    $this->paymentManager = $payment_manager;
  }

  /**
   * Perform the access check.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access check result.
   */
  public function access(PaymentGatewayInterface $payment_gateway, Request $request): AccessResultInterface {
    $query = $request->query;

    // Check the payment gateway.
    if (!$payment_gateway->getPlugin() instanceof VivawalletPaymentGatewayInterface) {
      return AccessResult::forbidden('Invalid payment gateway');
    }

    // Check if all query parameters are present.
    if (!$query->has('t') || !$query->has('s')) {
      return AccessResult::forbidden('Required query parameter missing');
    }

    // Validate the values.
    if (!Uuid::isValid($query->get('t'))) {
      return AccessResult::forbidden('Value "t" parameter invalid');
    }

    $value = $query->get('s');
    if ($value === '' || !ctype_digit($value)) {
      return AccessResult::forbidden('Value "s" parameter invalid');
    }

    // Check if the payment exists.
    $payment = $this->paymentManager->loadByOrderCodeOrTransactionId(
      $payment_gateway,
      $query->get('s'),
      $query->get('t')
    );

    if (!$payment) {
      return AccessResult::forbidden('Nonexistent payment');
    }

    return AccessResult::allowed();
  }

}
