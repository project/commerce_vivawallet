<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Exception\HttpResponseException;
use Drupal\commerce_vivawallet\Service\Http\Message\JsonDataDataStream;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * HTTP middleware to decode a JSON response.
 */
class JsonDecoder extends MiddlewareBase {

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $handler): callable {
    return function (RequestInterface $request, array $options) use ($handler) {
      $promise = $handler($request, $options);

      return $promise->then(
        function (ResponseInterface $response) use ($request, $options) {
          $body = $response->getBody();

          try {
            $data = json_decode($body->getContents(), TRUE, 1024, JSON_THROW_ON_ERROR);
          }
          catch (\JsonException $ex) {
            throw new HttpResponseException($request, $options, $response, $ex);
          }

          $body->rewind();

          return $response->withBody(new JsonDataDataStream($body, $data));
        }
      );
    };
  }

}
