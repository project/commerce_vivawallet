<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Psr\Http\Message\RequestInterface;

/**
 * HTTP middleware to add basic authorization.
 */
class BasicAuthorization extends MiddlewareBase {

  /**
   * The basic authorization.
   *
   * @var string
   */
  protected string $authorization;

  /**
   * {@inheritdoc}
   */
  public function configure(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): MiddlewareInterface {
    $instance = clone $this;
    $instance->authorization = base64_encode(
      $vivawallet_payment_gateway->getMerchantId() . ':' . $vivawallet_payment_gateway->getApiKey()
    );

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $handler): callable {
    return function (RequestInterface $request, array $options) use ($handler) {
      $request = $request->withHeader('Authorization', 'Basic ' . $this->authorization);

      return $handler($request, $options);
    };
  }

}
