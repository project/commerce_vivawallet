<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\commerce_vivawallet\Service\AccountServiceFactoryInterface;
use Drupal\commerce_vivawallet\Service\AccountServiceInterface;
use Psr\Http\Message\RequestInterface;

/**
 * HTTP middleware to add bearer authorization.
 */
class BearerAuthorization extends MiddlewareBase {

  /**
   * The account service factory.
   *
   * @var \Drupal\commerce_vivawallet\Service\AccountServiceFactoryInterface
   */
  protected AccountServiceFactoryInterface $accountServiceFactory;

  /**
   * The account service.
   *
   * @var \Drupal\commerce_vivawallet\Service\AccountServiceInterface
   */
  protected AccountServiceInterface $accountService;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\Service\AccountServiceFactoryInterface $account_service_factory
   *   The account service factory.
   */
  public function __construct(AccountServiceFactoryInterface $account_service_factory) {
    $this->accountServiceFactory = $account_service_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function configure(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): MiddlewareInterface {
    $instance = clone $this;
    $instance->accountService = $this->accountServiceFactory->get($vivawallet_payment_gateway);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $handler): callable {
    return function (RequestInterface $request, array $options) use ($handler) {
      $request = $request->withHeader('Authorization', 'Bearer ' . $this->accountService->getAccessToken());

      return $handler($request, $options);
    };
  }

}
