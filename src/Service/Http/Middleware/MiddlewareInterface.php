<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Provides an interface for HTTP client middleware.
 */
interface MiddlewareInterface {

  /**
   * Configure the middleware based on the Viva Wallet payment gateway.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Vivawallet payment gateway.
   *
   * @return \Drupal\commerce_vivawallet\Service\Http\Middleware\MiddlewareInterface
   *   The configured instance.
   */
  public function configure(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): MiddlewareInterface;

  /**
   * Call the middleware.
   *
   * @param callable $handler
   *   The handler.
   *
   * @return callable
   *   The callable stack.
   */
  public function __invoke(callable $handler): callable;

}
