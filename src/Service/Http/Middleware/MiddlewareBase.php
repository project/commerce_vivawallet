<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Base class for HTTP client middleware.
 */
abstract class MiddlewareBase implements MiddlewareInterface {

  /**
   * {@inheritdoc}
   */
  public function configure(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): MiddlewareInterface {
    return $this;
  }

}
