<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Exception\HttpRequestException;
use Drupal\commerce_vivawallet\Exception\HttpResponseException;
use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * HTTP middleware to log requests.
 */
class Logger extends MiddlewareBase {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Whether to log all requests.
   *
   * @var bool
   */
  protected bool $logAllRequests = FALSE;

  /**
   * Class constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function configure(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): MiddlewareInterface {
    $instance = clone $this;
    $instance->logAllRequests = $vivawallet_payment_gateway->logAllRequests();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $handler): callable {
    return function (RequestInterface $request, array $options) use ($handler) {
      $promise = $handler($request, $options);

      return $promise->then(
        function (ResponseInterface $response) use ($request, $options) {
          $this->logResponse($request, $options, $response);

          return $response;
        },
        function (\Exception $ex) use ($request, $options) {
          if (!$ex instanceof HttpResponseException) {
            $this->logError($request, $options, $ex);

            throw new HttpRequestException($request, $options, $ex);
          }

          if ($ex->getCode() >= 400) {
            $this->logError($request, $options, $ex);
          }
          else {
            $this->logResponse($request, $options, $ex->getResponse());
          }

          throw $ex;
        }
      );
    };
  }

  /**
   * Log a response.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   * @param array $options
   *   The request options.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The response.
   */
  protected function logResponse(RequestInterface $request, array $options, ResponseInterface $response): void {
    if (!$this->logAllRequests) {
      return;
    }

    $body = $response->getBody();

    $this->logger->info('@method request to @url succeeded.<br /><strong>Request options:</strong><br /><pre>@options</pre><br /><strong>Response status code:</strong> @status<br /><strong>Response body:</strong><br /><pre>@body</pre>', [
      '@method' => $request->getMethod(),
      '@url' => (string) $request->getUri(),
      '@options' => $this->renderOptions($options),
      '@status' => $response->getStatusCode(),
      '@body' => $body->getContents(),
    ]);

    $body->rewind();
  }

  /**
   * Log an error response.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   * @param array $options
   *   The request options.
   * @param \Exception $ex
   *   The exception.
   */
  protected function logError(RequestInterface $request, array $options, \Exception $ex): void {
    $context = [
      '@method' => $request->getMethod(),
      '@url' => (string) $request->getUri(),
      '@options' => $this->renderOptions($options),
      '@error' => $ex->getMessage(),
    ];

    if (!$ex instanceof HttpResponseException) {
      $this->logger->error('@method request to @url failed.<br /><strong>Request options:</strong><br /><pre>@options</pre><br /><strong>Error:</strong><br /><pre>@error</pre>', $context);
      return;
    }

    $response = $ex->getResponse();
    $body = $response->getBody();

    $context += [
      '@headers' => print_r($response->getHeaders(), TRUE),
      '@body' => $body->getContents(),
    ];

    $body->rewind();

    $this->logger->error('@method request to @url failed.<br /><strong>Request options:</strong><br /><pre>@options</pre><br /><strong>Response headers:</strong><br /><pre>@headers</pre><br /><strong>Response body:</strong><br /><pre>@body</pre><br /><strong>Error:</strong><br /><pre>@error</pre>', $context);
  }

  /**
   * Render the options for a log message.
   *
   * @param array $options
   *   The request options.
   *
   * @return string
   *   The rendered options.
   */
  protected function renderOptions(array $options): string {
    foreach ($options as $key => $value) {
      if (is_object($value)) {
        $options[$key] = '*** ' . get_class($value) . ' ***';
      }
    }

    return print_r($options, TRUE);
  }

}
