<?php

namespace Drupal\commerce_vivawallet\Service\Http\Middleware;

use Drupal\commerce_vivawallet\Exception\HttpResponseContentTypeException;
use Drupal\commerce_vivawallet\Exception\HttpResponseStatusCodeException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * HTTP middleware to validate the response headers.
 */
class HeaderValidator extends MiddlewareBase {

  /**
   * {@inheritdoc}
   */
  public function __invoke(callable $handler): callable {
    return function (RequestInterface $request, array $options) use ($handler) {
      $promise = $handler($request, $options);

      return $promise->then(
        function (ResponseInterface $response) use ($request, $options) {
          if ($response->getStatusCode() !== 200) {
            throw new HttpResponseStatusCodeException($request, $options, $response);
          }

          $content_type = $response->getHeaderLine('Content-Type');
          $content_type = explode(';', $content_type)[0] ?? '';

          if ($content_type !== 'application/json') {
            throw new HttpResponseContentTypeException($request, $options, $response);
          }

          return $response;
        }
      );
    };
  }

}
