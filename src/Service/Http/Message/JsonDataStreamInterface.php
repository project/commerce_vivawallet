<?php

namespace Drupal\commerce_vivawallet\Service\Http\Message;

use Psr\Http\Message\StreamInterface;

/**
 * Provides an interface for a JSON data stream.
 */
interface JsonDataStreamInterface extends StreamInterface {

  /**
   * Get the decoded JSON data.
   *
   * @return array
   *   The decoded JSON data.
   */
  public function getData(): array;

}
