<?php

namespace Drupal\commerce_vivawallet\Service\Http\Message;

use Psr\Http\Message\StreamInterface;

/**
 * Represents a JSON data stream.
 */
class JsonDataDataStream implements JsonDataStreamInterface {

  /**
   * The inner stream.
   *
   * @var \Psr\Http\Message\StreamInterface
   */
  protected StreamInterface $stream;

  /**
   * The decoded JSON data.
   *
   * @var array
   */
  protected array $data;

  /**
   * Class constructor.
   *
   * @param \Psr\Http\Message\StreamInterface $stream
   *   The inner stream.
   * @param array $data
   *   The decoded JSON data.
   */
  public function __construct(StreamInterface $stream, array $data) {
    $this->stream = $stream;
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString() {
    return (string) $this->stream;
  }

  /**
   * {@inheritdoc}
   */
  public function close() {
    $this->stream->close();
  }

  /**
   * {@inheritdoc}
   */
  public function detach() {
    return $this->stream->detach();
  }

  /**
   * {@inheritdoc}
   */
  public function getSize() {
    return $this->stream->getSize();
  }

  /**
   * {@inheritdoc}
   */
  public function tell() {
    return $this->stream->tell();
  }

  /**
   * {@inheritdoc}
   */
  public function eof() {
    return $this->stream->eof();
  }

  /**
   * {@inheritdoc}
   */
  public function isSeekable() {
    return $this->stream->isSeekable();
  }

  /**
   * {@inheritdoc}
   */
  public function seek($offset, $whence = SEEK_SET) {
    return $this->stream->seek($offset, $whence);
  }

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    $this->stream->rewind();
  }

  /**
   * {@inheritdoc}
   */
  public function isWritable() {
    return $this->stream->isWritable();
  }

  /**
   * {@inheritdoc}
   */
  public function write($string) {
    return $this->stream->write($string);
  }

  /**
   * {@inheritdoc}
   */
  public function isReadable() {
    return $this->stream->isReadable();
  }

  /**
   * {@inheritdoc}
   */
  public function read($length) {
    return $this->stream->read($length);
  }

  /**
   * {@inheritdoc}
   */
  public function getContents() {
    return $this->stream->getContents();
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata($key = NULL) {
    return $this->stream->getMetadata($key);
  }

}
