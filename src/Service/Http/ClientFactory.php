<?php

namespace Drupal\commerce_vivawallet\Service\Http;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\commerce_vivawallet\Service\Http\Middleware\MiddlewareInterface;
use Drupal\Core\Http\ClientFactory as CoreClientFactory;
use Drupal\Core\Site\Settings;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;

/**
 * Factory for HTTP clients.
 */
class ClientFactory implements ClientFactoryInterface {

  /**
   * Name of the service to create clients for.
   *
   * @var string
   */
  protected string $service;

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected CoreClientFactory $clientFactory;

  /**
   * The additional middleware.
   *
   * @var callable[]
   */
  protected array $middleware;

  /**
   * Class constructor.
   *
   * @param string $service
   *   Name of the service to create clients for.
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The HTTP client factory.
   * @param callable[] $middleware
   *   The additional middleware.
   */
  public function __construct(string $service, CoreClientFactory $client_factory, callable ...$middleware) {
    $this->service = $service;
    $this->clientFactory = $client_factory;
    $this->middleware = $middleware;
  }

  /**
   * {@inheritdoc}
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): ClientInterface {
    return $this->clientFactory->fromOptions([
      'base_uri' => $this->getBaseUrl($vivawallet_payment_gateway),
      'handler' => $this->createHandlerStack($vivawallet_payment_gateway),
      'http_errors' => FALSE,
      'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
      ],
    ]);
  }

  /**
   * Get the base URL.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return string
   *   The base URL.
   */
  protected function getBaseUrl(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): string {
    $mode = $vivawallet_payment_gateway->getMode();
    $url = NULL;

    switch ($this->service) {
      case 'account':
        $url = 'https://accounts.vivapayments.com/';

        if ($mode === 'test') {
          $url = 'https://demo-accounts.vivapayments.com/';
        }
        break;

      case 'api_oauth':
        $url = 'https://api.vivapayments.com/';

        if ($mode === 'test') {
          $url = 'https://demo-api.vivapayments.com/';
        }
        break;

      case 'api_basic':
        $url = 'https://www.vivapayments.com/api/';

        if ($mode === 'test') {
          $url = 'https://demo.vivapayments.com/api/';
        }
        break;
    }

    $url = Settings::get(
      'commerce_vivawallet_' . $this->service . '_url_' . $mode,
      $url
    );

    if (!is_string($url) || empty($url)) {
      throw new \InvalidArgumentException("No URL specified for {$this->service} in $mode mode");
    }

    return $url;
  }

  /**
   * Create the handler stack.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \GuzzleHttp\HandlerStack
   *   The handler stack.
   */
  protected function createHandlerStack(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): HandlerStack {
    $handler_stack = HandlerStack::create();

    foreach ($this->middleware as $middleware) {
      if ($middleware instanceof MiddlewareInterface) {
        $middleware = $middleware->configure($vivawallet_payment_gateway);
      }

      $handler_stack->push($middleware);
    }

    return $handler_stack;
  }

}
