<?php

namespace Drupal\commerce_vivawallet\Service\Http;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides an interface for the HTTP client factory.
 */
interface ClientFactoryInterface {

  /**
   * Get an HTTP client.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \GuzzleHttp\Client
   *   The HTTP client.
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): ClientInterface;

}
