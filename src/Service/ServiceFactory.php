<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Base class for service factories.
 */
abstract class ServiceFactory {

  /**
   * The HTTP client factory.
   *
   * @var \Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface
   */
  protected ClientFactoryInterface $clientFactory;

  /**
   * The service instances.
   *
   * @var array
   */
  protected array $instances = [];

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface $client_factory
   *   The HTTP client factory.
   */
  public function __construct(ClientFactoryInterface $client_factory) {
    $this->clientFactory = $client_factory;
  }

  /**
   * Get the HTTP client.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \GuzzleHttp\ClientInterface
   *   The HTTP client.
   */
  protected function getClient(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): ClientInterface {
    return $this->clientFactory->get($vivawallet_payment_gateway);
  }

  /**
   * Get the unique key for the instances array.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return string
   *   The unique key for the instances array.
   */
  protected function getInstanceKey(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): string {
    return sprintf(
      '%s %s %s',
      $vivawallet_payment_gateway->getClientId(),
      $vivawallet_payment_gateway->getClientSecret(),
      $vivawallet_payment_gateway->getMode()
    );
  }

}
