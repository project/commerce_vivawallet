<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides an interface for the order service.
 */
interface OrderServiceInterface {

  /**
   * Create a payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return int
   *   The unique order code.
   */
  public function createPayment(PaymentInterface $payment): int;

}
