<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;

/**
 * A factory to create the account service.
 */
class AccountServiceFactory extends ServiceFactory implements AccountServiceFactoryInterface {

  /**
   * The key value expirable factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface
   */
  protected KeyValueExpirableFactoryInterface $keyValueExpirableFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface $client_factory
   *   The HTTP client factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_expirable_factory
   *   The key value expirable factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ClientFactoryInterface $client_factory, KeyValueExpirableFactoryInterface $key_value_expirable_factory, TimeInterface $time) {
    parent::__construct($client_factory);

    $this->keyValueExpirableFactory = $key_value_expirable_factory;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): AccountServiceInterface {
    $key = $this->getInstanceKey($vivawallet_payment_gateway);

    if (isset($this->instances[$key])) {
      return $this->instances[$key];
    }

    $this->instances[$key] = new AccountService(
      $vivawallet_payment_gateway->getClientId(),
      $vivawallet_payment_gateway->getClientSecret(),
      $vivawallet_payment_gateway->getMode(),
      $this->getClient($vivawallet_payment_gateway),
      $this->keyValueExpirableFactory->get('commerce_vivawallet'),
      $this->time
    );

    return $this->instances[$key];
  }

}
