<?php

namespace Drupal\commerce_vivawallet\Service;

/**
 * Provides an interface for the config service.
 */
interface ConfigServiceInterface {

  /**
   * Get the token.
   *
   * @return string
   *   The configuration token.
   */
  public function getToken(): string;

}
