<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Provides an interface for the order service factory.
 */
interface OrderServiceFactoryInterface {

  /**
   * Get the order service.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \Drupal\commerce_vivawallet\Service\OrderServiceInterface
   *   The order service.
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): OrderServiceInterface;

}
