<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Provides an interface for the account service factory.
 */
interface AccountServiceFactoryInterface {

  /**
   * Get the account service.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \Drupal\commerce_vivawallet\Service\AccountServiceInterface
   *   The account service.
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): AccountServiceInterface;

}
