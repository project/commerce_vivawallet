<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Provides an interface for the transaction service factory.
 */
interface TransactionServiceFactoryInterface {

  /**
   * Get the transaction service.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \Drupal\commerce_vivawallet\Service\TransactionServiceInterface
   *   The transaction service.
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): TransactionServiceInterface;

}
