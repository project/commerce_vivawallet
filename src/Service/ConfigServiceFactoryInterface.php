<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * Provides an interface for the config service factory.
 */
interface ConfigServiceFactoryInterface {

  /**
   * Get the config service.
   *
   * @param \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway
   *   The Viva Wallet payment gateway.
   *
   * @return \Drupal\commerce_vivawallet\Service\ConfigServiceInterface
   *   The config service.
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): ConfigServiceInterface;

}
