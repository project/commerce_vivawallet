<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Exception\TokenRequestFailedException;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use GuzzleHttp\ClientInterface;

/**
 * The account service.
 */
class AccountService implements AccountServiceInterface {

  /**
   * The client ID.
   *
   * @var string
   */
  protected string $clientId;

  /**
   * The client secret.
   *
   * @var string
   */
  protected string $clientSecret;

  /**
   * The mode.
   *
   * @var string
   */
  protected string $mode;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * The key value store expirable.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $keyValueStoreExpirable;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The storage key.
   *
   * @var string
   */
  protected string $storageKey;

  /**
   * The access token.
   *
   * @var string
   */
  protected string $accessToken;

  /**
   * The expiration timestamp.
   *
   * @var int
   */
  protected int $expires;

  /**
   * Class constructor.
   *
   * @param string $client_id
   *   The client ID.
   * @param string $client_secret
   *   The client secret.
   * @param string $mode
   *   The mode.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $key_value_store_expirable
   *   The key value store expirable.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(string $client_id, string $client_secret, string $mode, ClientInterface $client, KeyValueStoreExpirableInterface $key_value_store_expirable, TimeInterface $time) {
    $this->clientId = $client_id;
    $this->clientSecret = $client_secret;
    $this->mode = $mode;
    $this->client = $client;
    $this->keyValueStoreExpirable = $key_value_store_expirable;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(bool $force_renew = FALSE): string {
    if ($force_renew) {
      return $this->requestAccessToken();
    }

    if (!isset($this->accessToken) && !$this->loadAccessToken()) {
      return $this->requestAccessToken();
    }

    if ($this->expires < $this->time->getCurrentTime()) {
      return $this->requestAccessToken();
    }

    return $this->accessToken;
  }

  /**
   * Load the stored access token.
   *
   * @return bool
   *   TRUE if the token was loaded.
   */
  protected function loadAccessToken(): bool {
    unset($this->accessToken, $this->expires);

    $key = $this->getKeyValueKey();

    if (!$this->keyValueStoreExpirable->has($key)) {
      return FALSE;
    }

    $value = $this->keyValueStoreExpirable->get($key);
    $this->accessToken = $value['access_token'];
    $this->expires = $value['expires'];

    return TRUE;
  }

  /**
   * Request a new access token.
   *
   * @return string
   *   The access token.
   */
  protected function requestAccessToken(): string {
    unset($this->accessToken, $this->expires);

    // Request the token.
    $now = $this->time->getCurrentTime();

    try {
      $response = $this->client->request('POST', 'connect/token', [
        'auth' => [$this->clientId, $this->clientSecret],
        'form_params' => [
          'grant_type' => 'client_credentials',
        ],
      ]);
    }
    catch (\Exception $ex) {
      throw new TokenRequestFailedException($ex);
    }

    /** @var \Drupal\commerce_vivawallet\Service\Http\Message\JsonDataStreamInterface $response */
    $response = $response->getBody();
    $response = $response->getData();

    // Set the token and expires time.
    $this->accessToken = $response['access_token'];
    $this->expires = $now + $response['expires_in'];

    // Calculate how long the token is valid and save it to the storage.
    $expire = $this->expires - $this->time->getCurrentTime();

    $this->keyValueStoreExpirable->setWithExpire($this->getKeyValueKey(), [
      'access_token' => $response['access_token'],
      'expires' => $response['expires_in'] + $now,
    ], $expire);

    return $this->accessToken;
  }

  /**
   * Get the key for the key value storage.
   *
   * @return string
   *   The key value key.
   */
  protected function getKeyValueKey(): string {
    if (!isset($this->storageKey)) {
      $this->storageKey = sha1($this->clientId . ' ' . $this->clientSecret . ' ' . $this->mode);
    }

    return $this->storageKey;
  }

}
