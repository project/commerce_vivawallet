<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * A factory to create the transaction service.
 */
class TransactionServiceFactory extends ServiceFactory implements TransactionServiceFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): TransactionServiceInterface {
    $key = $this->getInstanceKey($vivawallet_payment_gateway);

    if (isset($this->instances[$key])) {
      return $this->instances[$key];
    }

    $this->instances[$key] = new TransactionService(
      $this->getClient($vivawallet_payment_gateway)
    );

    return $this->instances[$key];
  }

}
