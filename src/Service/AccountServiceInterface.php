<?php

namespace Drupal\commerce_vivawallet\Service;

/**
 * Provides an interface for the account service.
 */
interface AccountServiceInterface {

  /**
   * Get an access token.
   *
   * @param bool $force_renew
   *   Set to TRUE to force a new token.
   *
   * @return string
   *   The access token.
   *
   * @throws \Drupal\commerce_vivawallet\Exception\TokenRequestFailedException
   */
  public function getAccessToken(bool $force_renew = FALSE): string;

}
