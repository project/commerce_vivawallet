<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;

/**
 * A factory to create the config service.
 */
class ConfigServiceFactory extends ServiceFactory implements ConfigServiceFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): ConfigServiceInterface {
    $key = $this->getInstanceKey($vivawallet_payment_gateway);

    if (isset($this->instances[$key])) {
      return $this->instances[$key];
    }

    $this->instances[$key] = new ConfigService(
      $this->getClient($vivawallet_payment_gateway)
    );

    return $this->instances[$key];
  }

}
