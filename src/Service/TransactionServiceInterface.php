<?php

namespace Drupal\commerce_vivawallet\Service;

/**
 * Provides an interface for the transaction service.
 */
interface TransactionServiceInterface {

  /**
   * Get the transaction.
   *
   * @param string $transaction_id
   *   The transaction ID.
   *
   * @return array
   *   The transaction data.
   *
   * @throws \Drupal\commerce_vivawallet\Exception\TransactionNotFoundException
   */
  public function get(string $transaction_id): array;

}
