<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use GuzzleHttp\ClientInterface;

/**
 * The order service.
 */
class OrderService implements OrderServiceInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * The source code.
   *
   * @var string
   */
  protected string $sourceCode;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Class constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   * @param string $source_code
   *   The source code.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ClientInterface $client, string $source_code, TimeInterface $time, LanguageManagerInterface $language_manager) {
    $this->client = $client;
    $this->sourceCode = $source_code;
    $this->time = $time;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment): int {
    $response = $this->client->request('POST', 'checkout/v2/orders', [
      'json' => $this->buildOrder($payment),
    ]);

    /** @var \Drupal\commerce_vivawallet\Service\Http\Message\JsonDataStreamInterface $response */
    $response = $response->getBody();
    $response = $response->getData();

    return $response['orderCode'];
  }

  /**
   * Build the order data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   *
   * @return array
   *   The order data.
   */
  protected function buildOrder(PaymentInterface $payment): array {
    // Calculate the timeout.
    $timeout = $payment->getExpiresTime() ?: 0;

    if ($timeout) {
      $timeout -= $this->time->getCurrentTime();
    }

    if ($timeout < 1 || $timeout > 65535) {
      $timeout = 65535;
    }

    // Get order and store name.
    $order = $payment->getOrder();
    $store = $order->getStore()->getName();

    // Return the order data.
    return [
      'amount' => $payment->getAmount()->multiply(100)->getNumber(),
      'customer' => $this->buildCustomer($order),
      'paymentTimeOut' => $timeout,
      'disableCash' => TRUE,
      'disableWallet' => TRUE,
      'sourceCode' => $this->sourceCode,
      'merchantTrns' => sprintf(
        'Order %u (%s)',
        $order->id(),
        $store,
      ),
      'tags' => [
        'Drupal Commerce',
        $store,
      ],
    ];
  }

  /**
   * Build the customer data.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return array
   *   The customer data.
   */
  protected function buildCustomer(OrderInterface $order): array {
    /** @var \Drupal\address\AddressInterface $address */
    $address = $order->getBillingProfile()->get('address')->first();

    $customer = [
      'email' => $order->getEmail(),
      'fullName' => $address->getGivenName() . ' ' . $address->getFamilyName(),
      'countryCode' => $address->getCountryCode(),
    ];

    $request_langcode = $this->getRequestLangcode($address->getCountryCode());

    if ($request_langcode) {
      $customer['requestLang'] = $request_langcode;
    }

    return $customer;
  }

  /**
   * Get the request langcode.
   *
   * @param string|null $country
   *   The ISO 3166-1 alpha-2 country code.
   * @param string|null $langcode
   *   ISO 639-1 language code.
   *
   * @return string|null
   *   The request language code or NULL uf unknown.
   */
  protected function getRequestLangcode(string $country = NULL, string $langcode = NULL): ?string {
    $existing_langcodes = [
      'bg' => 'bg-BG',
      'hr' => 'hr-HR',
      'cs' => 'cs-CZ',
      'da' => 'da-DK',
      'nl' => 'nl-NL',
      'en' => 'en-GB',
      'fi' => 'fi-FI',
      'fr' => 'fr-FR',
      'de' => 'de-DE',
      'el' => 'el-GR',
      'hu' => 'hu-HU',
      'it' => 'it-IT',
      'pl' => 'pl-PL',
      'pt' => 'pt-PT',
      'ro' => 'ro-RO',
      'es' => 'es-ES',
    ];

    $langcode = $langcode ?? $this->languageManager->getCurrentLanguage()->getId();

    if ($country) {
      $langcode_country = $langcode . '-' . $country;

      if (in_array($langcode, $existing_langcodes, TRUE)) {
        return $langcode_country;
      }
    }

    return $existing_langcodes[$langcode] ?? NULL;
  }

}
