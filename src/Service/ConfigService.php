<?php

namespace Drupal\commerce_vivawallet\Service;

use GuzzleHttp\ClientInterface;

/**
 * The config service.
 */
class ConfigService implements ConfigServiceInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * Class constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken(): string {
    $response = $this->client->request('GET', 'messages/config/token');

    /** @var \Drupal\commerce_vivawallet\Service\Http\Message\JsonDataStreamInterface $response */
    $response = $response->getBody();

    return $response->getData()['Key'];
  }

}
