<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Exception\HttpResponseStatusCodeException;
use Drupal\commerce_vivawallet\Exception\TransactionNotFoundException;
use GuzzleHttp\ClientInterface;

/**
 * The transaction service.
 */
class TransactionService implements TransactionServiceInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * Class constructor.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $transaction_id): array {
    try {
      $response = $this->client->request('GET', 'checkout/v2/transactions/' . $transaction_id);
    }
    catch (HttpResponseStatusCodeException $ex) {
      if ($ex->getCode() === 404) {
        throw new TransactionNotFoundException($transaction_id);
      }

      throw $ex;
    }

    /** @var \Drupal\commerce_vivawallet\Service\Http\Message\JsonDataStreamInterface $response */
    $response = $response->getBody();

    return $response->getData();
  }

}
