<?php

namespace Drupal\commerce_vivawallet\Service;

use Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface;
use Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * A factory to create the order service.
 */
class OrderServiceFactory extends ServiceFactory implements OrderServiceFactoryInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\Service\Http\ClientFactoryInterface $client_factory
   *   The HTTP client factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(ClientFactoryInterface $client_factory, TimeInterface $time, LanguageManagerInterface $language_manager) {
    parent::__construct($client_factory);

    $this->time = $time;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function get(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): OrderServiceInterface {
    $key = $this->getInstanceKey($vivawallet_payment_gateway);

    if (isset($this->instances[$key])) {
      return $this->instances[$key];
    }

    $this->instances[$key] = new OrderService(
      $this->getClient($vivawallet_payment_gateway),
      $vivawallet_payment_gateway->getSourceCode(),
      $this->time,
      $this->languageManager
    );

    return $this->instances[$key];
  }

  /**
   * {@inheritdoc}
   */
  protected function getInstanceKey(VivawalletPaymentGatewayInterface $vivawallet_payment_gateway): string {
    return parent::getInstanceKey($vivawallet_payment_gateway) . ' ' . $vivawallet_payment_gateway->getSourceCode();
  }

}
