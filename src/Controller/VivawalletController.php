<?php

namespace Drupal\commerce_vivawallet\Controller;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_vivawallet\PaymentManagerInterface;
use Drupal\commerce_vivawallet\Service\ConfigServiceFactoryInterface;
use Drupal\commerce_vivawallet\Service\TransactionServiceFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to handle customers that return from Viva Wallet.
 */
class VivawalletController extends ControllerBase {

  /**
   * The payment manager.
   *
   * @var \Drupal\commerce_vivawallet\PaymentManagerInterface
   */
  protected PaymentManagerInterface $paymentManager;

  /**
   * The transaction service factory.
   *
   * @var \Drupal\commerce_vivawallet\Service\TransactionServiceFactoryInterface
   */
  protected TransactionServiceFactoryInterface $transactionServiceFactory;

  /**
   * The config service factory.
   *
   * @var \Drupal\commerce_vivawallet\Service\ConfigServiceFactoryInterface
   */
  protected ConfigServiceFactoryInterface $configServiceFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\PaymentManagerInterface $payment_manager
   *   The payment manager.
   * @param \Drupal\commerce_vivawallet\Service\TransactionServiceFactoryInterface $transaction_service_factory
   *   The transaction service factory.
   * @param \Drupal\commerce_vivawallet\Service\ConfigServiceFactoryInterface $config_service_factory
   *   The config service factory.
   */
  public function __construct(PaymentManagerInterface $payment_manager, TransactionServiceFactoryInterface $transaction_service_factory, ConfigServiceFactoryInterface $config_service_factory) {
    $this->paymentManager = $payment_manager;
    $this->transactionServiceFactory = $transaction_service_factory;
    $this->configServiceFactory = $config_service_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_vivawallet.payment_manager'),
      $container->get('commerce_vivawallet.transaction_service.factory'),
      $container->get('commerce_vivawallet.config_service.factory')
    );
  }

  /**
   * Handle a successfull payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function success(PaymentGatewayInterface $payment_gateway, Request $request): RedirectResponse {
    $payment = $this->getPaymentFromQuery($payment_gateway, $request->query);

    try {
      $this->processTransaction($payment, $this->getTransactionIdFromQuery($request->query));
    }
    catch (\InvalidArgumentException $ex) {
      throw new NotFoundHttpException($ex->getMessage(), $ex);
    }

    return $this->redirect('commerce_payment.checkout.return', [
      'commerce_order' => $payment->getOrderId(),
      'step' => 'payment',
    ]);
  }

  /**
   * Handle a cancelled payment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function cancel(PaymentGatewayInterface $payment_gateway, Request $request): RedirectResponse {
    $payment = $this->getPaymentFromQuery($payment_gateway, $request->query);

    try {
      $this->processTransaction($payment, $this->getTransactionIdFromQuery($request->query));
    }
    catch (\InvalidArgumentException $ex) {
      throw new NotFoundHttpException($ex->getMessage(), $ex);
    }

    return $this->redirect('commerce_payment.checkout.cancel', [
      'commerce_order' => $payment->getOrderId(),
      'step' => 'payment',
    ]);
  }

  /**
   * Handle the transaction payment created hook.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The HTML response.
   */
  public function hook(PaymentGatewayInterface $payment_gateway, Request $request): HtmlResponse {
    $content = json_decode($request->getContent(), TRUE, 1024, JSON_THROW_ON_ERROR);
    $order_code = $content['EventData']['OrderCode'];
    $transaction_id = $content['EventData']['TransactionId'];

    $payment = $this->paymentManager->loadByOrderCodeOrTransactionId($payment_gateway, $order_code, $transaction_id);

    $this->getLogger('commerce_vivawallet')->info('Feedback hook triggered for payment @payment of order @order', [
      '@payment' => $payment->id(),
      '@order' => $payment->getOrder()->getOrderNumber() ?: $payment->getOrderId(),
    ]);

    try {
      $this->processTransaction($payment, $transaction_id);
    }
    catch (\InvalidArgumentException $ex) {
      throw new NotFoundHttpException($ex->getMessage(), $ex);
    }

    return new HtmlResponse('');
  }

  /**
   * Handles hook verification by Viva Wallet.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function verifyHook(PaymentGatewayInterface $payment_gateway) {
    $this->getLogger('commerce_vivawallet')->info('Hook verification triggered');

    /** @var \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $plugin */
    $plugin = $payment_gateway->getPlugin();
    $config_service = $this->configServiceFactory->get($plugin);
    $token = $config_service->getToken();

    return new JsonResponse([
      'key' => $token,
    ]);
  }

  /**
   * Get the transaction ID from the query parameters.
   *
   * @param \Symfony\Component\HttpFoundation\ParameterBag $query
   *   The query parameter bag.
   *
   * @return string
   *   The transaction ID.
   */
  protected function getTransactionIdFromQuery(ParameterBag $query): string {
    return $query->get('t');
  }

  /**
   * Get the payment entity from the query parameters.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway entity.
   * @param \Symfony\Component\HttpFoundation\ParameterBag $query
   *   The query parameter bag.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment entity.
   */
  protected function getPaymentFromQuery(PaymentGatewayInterface $payment_gateway, ParameterBag $query): PaymentInterface {
    return $this->paymentManager->loadByOrderCodeOrTransactionId(
      $payment_gateway,
      $query->get('s'),
      $this->getTransactionIdFromQuery($query)
    );
  }

  /**
   * Process a transaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   * @param string $transaction_id
   *   The transaction ID.
   */
  protected function processTransaction(PaymentInterface $payment, string $transaction_id): void {
    /** @var \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway */
    $vivawallet_payment_gateway = $payment->getPaymentGateway()->getPlugin();
    $transaction_service = $this->transactionServiceFactory->get($vivawallet_payment_gateway);
    $transaction = $transaction_service->get($transaction_id);

    // Ensure the payment and transaction are linked.
    $remote_id = $payment->getRemoteId();
    if ($remote_id !== $transaction_id && $remote_id !== (string) $transaction['orderCode']) {
      throw new \InvalidArgumentException('Remote ID payment does not match order code');
    }

    // Update the payment.
    $payment->setRemoteId($transaction_id);
    $payment->setRemoteState($transaction['statusId']);

    switch ($transaction['statusId']) {
      case 'A':
      case 'M':
      case 'MA':
      case 'MI':
      case 'MW':
      case 'MS':
        $payment->setState('authorization');
        break;

      case 'X':
      case 'ML':
      case 'E':
        $payment->setState('authorization_voided');
        break;

      case 'C':
      case 'F':
        $payment->setAuthorizedTime(strtotime($transaction['insDate']));
        $payment->setState('completed');
        break;

      case 'R':
        $payment->setState('refunded');
        break;
    }

    $payment->save();
  }

}
