<?php

namespace Drupal\commerce_vivawallet\Exception;

/**
 * Exception thrown when a transaction could not be found.
 */
class TransactionNotFoundException extends \InvalidArgumentException {

  /**
   * The transaction ID.
   *
   * @var string
   */
  protected string $transactionId;

  /**
   * Class constructor.
   *
   * @param string $transaction_id
   *   The transaction ID.
   */
  public function __construct(string $transaction_id) {
    parent::__construct("Transaction $transaction_id not found");

    $this->transactionId = $transaction_id;
  }

  /**
   * Get the transaction ID.
   *
   * @return string
   *   The transaction ID.
   */
  public function getTransactionId(): string {
    return $this->transactionId;
  }

}
