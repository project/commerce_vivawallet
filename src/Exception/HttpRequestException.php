<?php

namespace Drupal\commerce_vivawallet\Exception;

use Psr\Http\Message\RequestInterface;

/**
 * Exception thrown when a request fails.
 */
class HttpRequestException extends HttpException {

  /**
   * Class constructor.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   * @param array $options
   *   The Guzzle request options.
   * @param \Throwable $previous
   *   The previous exception.
   * @param string|null $message
   *   The message.
   */
  public function __construct(RequestInterface $request, array $options, \Throwable $previous = NULL, string $message = NULL) {
    if ($message === NULL) {
      $message = $request->getMethod() . ' request to ' . $request->getUri() . ' failed';

      if ($previous) {
        $message .= ': ' . $previous->getMessage();
      }
    }

    parent::__construct($message, 0, $request, $options, $previous);
  }

}
