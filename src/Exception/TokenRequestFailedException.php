<?php

namespace Drupal\commerce_vivawallet\Exception;

/**
 * Exception thrown when a token request failed.
 */
class TokenRequestFailedException extends \RuntimeException {

  /**
   * Class constructor.
   *
   * @param \Throwable $previous
   *   The previous exception causing this one.
   */
  public function __construct(\Throwable $previous) {
    parent::__construct($previous->getMessage(), 0, $previous);
  }

}
