<?php

namespace Drupal\commerce_vivawallet\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Exception thrown when an unexpected content type is received.
 */
class HttpResponseContentTypeException extends HttpResponseException {

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestInterface $request, array $options, ResponseInterface $response) {
    $content_type = $response->getHeaderLine('Content-Type');
    $message = 'Content type "' . $content_type . '" was unexpected for ' . $request->getMethod() . ' request to ' . $request->getUri();

    parent::__construct($request, $options, $response, NULL, $message);
  }

}
