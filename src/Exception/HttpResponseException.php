<?php

namespace Drupal\commerce_vivawallet\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Exception thrown when an invalid response is received.
 */
class HttpResponseException extends HttpException {

  /**
   * The response.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected ResponseInterface $response;

  /**
   * Class constructor.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   * @param array $options
   *   The Guzzle request options.
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The response.
   * @param \Throwable|null $previous
   *   The previous exception.
   * @param string|null $message
   *   The message.
   */
  public function __construct(RequestInterface $request, array $options, ResponseInterface $response, \Throwable $previous = NULL, string $message = NULL) {
    if ($message === NULL) {
      $message = 'Unexpected response for ' . $request->getMethod() . ' request to ' . $request->getUri();

      if ($previous) {
        $message .= ': ' . $previous->getMessage();
      }
    }

    parent::__construct($message, $response->getStatusCode(), $request, $options, $previous);

    $this->response = $response;
  }

  /**
   * Get response.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  public function getResponse(): ResponseInterface {
    return $this->response;
  }

}
