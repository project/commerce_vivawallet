<?php

namespace Drupal\commerce_vivawallet\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Exception thrown when an unexpected status code is received.
 */
class HttpResponseStatusCodeException extends HttpResponseException {

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestInterface $request, array $options, ResponseInterface $response) {
    $status = $response->getStatusCode();
    $message = 'Status code ' . $status . ' was unexpected for ' . $request->getMethod() . ' request to ' . $request->getUri();

    parent::__construct($request, $options, $response, NULL, $message);
  }

}
