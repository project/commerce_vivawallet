<?php

namespace Drupal\commerce_vivawallet\Exception;

use Psr\Http\Message\RequestInterface;

/**
 * Base class for all HTTP exceptions.
 */
abstract class HttpException extends \RuntimeException {

  /**
   * The request.
   *
   * @var \Psr\Http\Message\RequestInterface
   */
  protected RequestInterface $request;

  /**
   * The Guzzle request options.
   *
   * @var array
   */
  protected array $options;

  /**
   * Class constructor.
   *
   * @param string $message
   *   The message.
   * @param int $code
   *   The code.
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   * @param array $options
   *   The Guzzle request options.
   * @param \Throwable|null $previous
   *   The previous exception.
   */
  public function __construct(string $message, int $code, RequestInterface $request, array $options, \Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);

    $this->request = $request;
    $this->options = $options;
  }

  /**
   * Get the request.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   The request.
   */
  public function getRequest(): RequestInterface {
    return $this->request;
  }

  /**
   * Get the Guzzle request options.
   *
   * @return array
   *   The Guzzle request options.
   */
  public function getOptions(): array {
    return $this->options;
  }

}
