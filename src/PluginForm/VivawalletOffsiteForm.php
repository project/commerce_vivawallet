<?php

namespace Drupal\commerce_vivawallet\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_vivawallet\Service\OrderServiceFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the offsite form for Viva Wallet.
 */
class VivawalletOffsiteForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The order service factory.
   *
   * @var \Drupal\commerce_vivawallet\Service\OrderServiceFactoryInterface
   */
  protected OrderServiceFactoryInterface $orderServiceFactory;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\commerce_vivawallet\Service\OrderServiceFactoryInterface $order_service_factory
   *   The order service factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(OrderServiceFactoryInterface $order_service_factory, TimeInterface $time) {
    $this->orderServiceFactory = $order_service_factory;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_vivawallet.order_service.factory'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway\VivawalletPaymentGatewayInterface $vivawallet_payment_gateway */
    $vivawallet_payment_gateway = $payment->getPaymentGateway()->getPlugin();

    if (!$payment->getRemoteId()) {
      // Set the expires time.
      if (!$payment->getExpiresTime()) {
        $payment->setExpiresTime($this->time->getCurrentTime() + 3600);
      }

      // Create the remote payment.
      $payment->setRemoteId(
        $this->orderServiceFactory
          ->get($vivawallet_payment_gateway)
          ->createPayment($payment)
      );

      // Save the updated payment.
      $payment->save();
    }

    $redirect_url = $this->getRedirectUrl($vivawallet_payment_gateway->getMode());

    return $this->buildRedirectForm($form, $form_state, $redirect_url, [
      'ref' => $payment->getRemoteId(),
      'color' => $vivawallet_payment_gateway->getColor(),
    ]);
  }

  /**
   * Get the redirect URL.
   *
   * @param string $mode
   *   The mode in which the payment gateway is operating.
   *
   * @return string
   *   The redirect URL.
   */
  protected function getRedirectUrl(string $mode): string {
    if ($mode === 'test') {
      $default = 'https://demo.vivapayments.com/web/checkout';
    }
    else {
      $default = 'https://www.vivapayments.com/web/checkout';
    }

    return Settings::get('commerce_vivawallet_redirect_url_' . $mode, $default);
  }

}
