<?php

namespace Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface;

/**
 * Provides an interface for the Viva Wallet payment gateway.
 */
interface VivawalletPaymentGatewayInterface extends PaymentGatewayInterface {

  /**
   * Get the merchant ID.
   *
   * @return string
   *   The merchant ID.
   */
  public function getMerchantId(): string;

  /**
   * Get the API key.
   *
   * @return string
   *   The API key.
   */
  public function getApiKey(): string;

  /**
   * Get the client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId(): string;

  /**
   * Get the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret(): string;

  /**
   * Get the source code.
   *
   * @return string
   *   The source code.
   */
  public function getSourceCode(): string;

  /**
   * Check if all HTTP requests should be logged.
   *
   * @return bool
   *   TRUE if all requests should be logged, FALSE otherwise.
   */
  public function logAllRequests(): bool;

  /**
   * Get the hexadecimal color.
   *
   * @return string
   *   The hexadecimal color.
   */
  public function getColor(): string;

}
