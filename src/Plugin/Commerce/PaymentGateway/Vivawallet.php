<?php

namespace Drupal\commerce_vivawallet\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the Viva Wallet payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "vivawallet",
 *   label = @Translation("Viva Wallet"),
 *   display_label = @Translation("Viva Wallet"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_vivawallet\PluginForm\VivawalletOffsiteForm",
 *   },
 *   payment_method_types = {"vivawallet"},
 *   requires_billing_information = TRUE,
 * )
 */
class Vivawallet extends OffsitePaymentGatewayBase implements VivawalletPaymentGatewayInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = [
      'test' => [
        'client_id' => '',
        'client_secret' => '',
        'source_code' => 'Default',
        'log_all_requests' => TRUE,
      ],
      'live' => [
        'client_id' => '',
        'client_secret' => '',
        'source_code' => 'Default',
        'log_all_requests' => FALSE,
      ],
      'color' => '047f8f',
    ];

    return $config + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    foreach ($this->getSupportedModes() as $mode => $label) {
      $form[$mode] = [
        '#type' => 'details',
        '#title' => $label,
        '#open' => TRUE,
      ];

      $form[$mode]['merchant_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Merchant ID'),
        '#default_value' => $this->configuration[$mode]['merchant_id'],
        '#required' => TRUE,
      ];

      $form[$mode]['api_key'] = [
        '#type' => 'textfield',
        '#title' => $this->t('API key'),
        '#default_value' => $this->configuration[$mode]['api_key'],
        '#required' => TRUE,
      ];

      $form[$mode]['client_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Client ID'),
        '#default_value' => $this->configuration[$mode]['client_id'],
        '#required' => TRUE,
      ];

      $form[$mode]['client_secret'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Client secret'),
        '#default_value' => $this->configuration[$mode]['client_secret'],
        '#required' => TRUE,
      ];

      $form[$mode]['source_code'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Source code'),
        '#default_value' => $this->configuration[$mode]['source_code'],
        '#required' => TRUE,
      ];

      $form[$mode]['log_all_requests'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Log all requests'),
        '#default_value' => (int) $this->configuration[$mode]['log_all_requests'],
      ];
    }

    $form['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Color'),
      '#default_value' => $this->configuration['color'],
      '#maxlength' => 8,
      '#size' => 10,
      '#field_prefix' => '#',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    foreach ($this->getSupportedModes() as $mode => $label) {
      $this->configuration[$mode] = $values[$mode];
    }

    $this->configuration['color'] = $values['color'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantId(): string {
    return $this->getConfiguration()[$this->getMode()]['merchant_id'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey(): string {
    return $this->getConfiguration()[$this->getMode()]['api_key'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getClientId(): string {
    return $this->getConfiguration()[$this->getMode()]['client_id'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret(): string {
    return $this->getConfiguration()[$this->getMode()]['client_secret'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceCode(): string {
    return $this->getConfiguration()[$this->getMode()]['source_code'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function logAllRequests(): bool {
    return $this->getConfiguration()[$this->getMode()]['log_all_requests'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getColor(): string {
    return $this->getConfiguration()['color'] ?? '';
  }

}
