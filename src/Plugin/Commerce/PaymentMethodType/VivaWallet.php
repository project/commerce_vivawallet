<?php

namespace Drupal\commerce_vivawallet\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the Viva Wallet payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "vivawallet",
 *   label = @Translation("Viva Wallet"),
 * )
 */
class VivaWallet extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->getLabel();
  }

}
